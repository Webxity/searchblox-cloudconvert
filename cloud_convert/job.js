'use strict';

const winston = require('winston');
const http = require('http');
const url = require('url');
const fs = require('fs');
const async = require('async');

let argv = require('minimist')(process.argv.slice(2));

const hlpr = require('./helpers');
const jobConfigFile = 'C:\\node-app\\job-config.json';

let config = {};
let finishTick = undefined;
let crashTick = undefined;

let cluster = require('cluster');
if (cluster.isMaster) {
  cluster.fork();

  cluster.on('exit', function(worker, code, signal) {
    cluster.fork();
  });
}

let job = {
  crashed: false,
  finished: false,
  start: () => {
    initLogger()
      .then(createConfig)
      .then(_run);
  },
  restart: () => {
    winston.info('Job is restarted.');
    _run();
  },
  stop: () => {
    process.kill(process.pid);
  }
};

if (cluster.isWorker) {
  if (argv.restart === true) {
    job.stop();
  }

  job.start();
}

if (argv.stop === true) {
  job.stop();
}

setInterval(() => {
  // 5 second delay before restarting, if job crashed softly
  if (job.crashed === true && !crashTick) {
    crashTick = setTimeout(() => {
      job.restart();
      crashTick = undefined;
    });
  }

  // restart in 10 minutes, if previous job completion succeeded
  if (job.finished === true && !finishTick) {
    finishTick = setTimeout(() => {
      resetConfig()
        .then(() => {
          job.restart();
          finishTick = undefined;
        });
    }, 600000);
  }
}, 5000);

function _run() {
  job.crashed = false;
  job.finished = false;

  return loadConfig()
    .then(checkSearchbloxServer)
    .then(startJob)
    .then(continueJob)
    .then(finishJob)
    .catch((err) => {
      job.crashed = true;
      winston.error(err);
      let errMsg = 'Job has stopped/crashed please see the <./logs/job-logs.log> file for more info, restarting in 5 seconds.';
      console.error(errMsg);
    });
}

function createConfig(force) {
  let defaults = {
    "params": {
      "query": "contenttype:pdf or contenttype:word or contenttype:excel or contenttype:html or contenttype:ppt",
      "page": 1,
      "pagesize": 10
    }
  };
  let defaultsString = JSON.stringify(defaults, null, 4);
    
  if (force === true) {
    return _createFile();
  }

  return hlpr.globSearch(jobConfigFile)
    .then(files => {
      if (!files.length) {
        return _createFile();
      }

      return Promise.resolve();
    })
    .catch(_createFile);

  function _createFile() {
    return new Promise((resolve, reject) => {
      fs.writeFile(jobConfigFile, defaultsString, writeFileFn);

      function writeFileFn(err) {
        if (err) {
          return reject(err);
        }

        winston.info(`${jobConfigFile} created.`);
        resolve();
      }
    });
  }
}

function loadConfig() {
  return hlpr.loadJSON(jobConfigFile)
    .then(data => {
      config = data;
      return Promise.resolve();
    })
    .catch(function() {
        return createConfig(true);
    });
}

function initLogger() {
  winston.cli();
  winston.add(winston.transports.DailyRotateFile, {
    filename: 'C:\\node-app\\logs\\job\\log',
    handleExceptions: true,
    humanReadableUnhandledException: true,
    json: false
  });

  return Promise.resolve();
}

function checkSearchbloxServer() {
  return new Promise((resolve, reject) => {
    let request = http.get(buildAPIUrl(), response);

    request.on('error', (err) => {
      winston.error('Searchblox service must be running. Exiting ..');
      return reject(err);
    });

    function response() {
      resolve();
    }
  });
}

function startJob() {
  return new Promise((resolve, reject) => {
    return httpRequest((err, data) => {
      if (err) {
        return reject(err);
      }

      resolve(data);
    });
  });
}

function continueJob(data) {
  return new Promise(_continueJob);

  function _continueJob(resolve, reject) {

    if (typeof data === 'object' && data.finish === true) {
      return resolve();
    }

    let lastPage = data.results['@lastpage'];

    async.eachSeries(data.results.result, iterator, callback);

    function iterator(result, cb) {
      let url = result['url'];

      if (!url) {
        winston.warn(`:url not found for result no: ${result['@no']}`);
        return cb(null);
      }
      hlpr.batchJob = true;
      hlpr.downloadFileAndProcess(url, true, true)
        .then(() => cb(null))
        .catch(() => cb(null));
    }

    function callback(err) {
      if (err) {
        return winston.error(err);
      }

      config.params.page++;
      config.totalPage = +lastPage;

      return saveJobConfig(config)
        .then(loadConfig)
        .then(startJob)
        .then(continueJob)
        .catch(reject);
    }
  }
}

function finishJob() {
  winston.info('Job finished, no further tasks found, running job again in 10 minutes.');
  job.finished = true;
}

function httpRequest(fn) {
  if ((config.params.page - config.totalPage) === 1) {
    fn(null, { finish: true });
  }

  let apiUrl = buildAPIUrl(config.params);
  let request = http.get(apiUrl, response);

  request.on('error', err => {
    winston.error(err);
    fn(err);
  });

  function response(res) {
    let body = '';
    res.on('data', chunk => body += chunk);

    res.on('end', () => {
      try {
        let _body = JSON.parse(body);
        fn(null, _body);
      } catch(e) {
        return fn(e);
      }
    });
  }
}

function buildAPIUrl(params) {
  let basicUrl = {
    'protocol': 'http',
    'host': 'localhost:8080',
    'pathname': 'searchblox/servlet/SearchServlet',
  };

  if (params && typeof params === 'object') {
    basicUrl.query = JSON.parse(JSON.stringify(params));
    basicUrl.query.xsl = 'json';
  }

  return url.format(basicUrl);
}

function saveJobConfig(config) {
  return new Promise((resolve, reject) => {
    fs.writeFile(jobConfigFile, JSON.stringify(config, null, 4), writeFileFn);

    function writeFileFn(err) {
      if (err) {
        return reject(err);
      }

      winston.info(`${jobConfigFile} updated!`);
      resolve();
    }
  });
}

function resetConfig() {
  return loadConfig()
    .then(() => {
      config.params.page = 1;
      return saveJobConfig(config);
    })
    .catch(err => winston.error(err));
}
