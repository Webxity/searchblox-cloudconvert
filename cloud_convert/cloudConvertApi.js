'use strict';

/*
* Module to handle all communication with the CloudConvert API
*/

module.exports = (SbConfig, hlpr) => {
	this.apiKey = SbConfig.apiKey;
	this.ZipFileName = "";

	const AdmZip = require('adm-zip');
	const winston = require('winston');
	const path = require('path');
	const fs = require('fs');
	const bl = require('bl');
	const fileType = require('file-type');
	const cloudconvert = new (require('cloudconvert'))(this.apiKey);

	return {
		uploadAndSaveConvertedFile: (filePath, fileName, type, inputExt, outputExt, targetExt, fileURL) => {
    		return new Promise((resolve, reject) => {
	    		inputExt = inputExt.replace("." , "");
	    		fileName = fileName.replace("." , "");
				var conversionOption={
				    "outputformat": outputExt,
				    "inputformat": inputExt,
				    "input": type,
				    "filename": fileName +"." +inputExt,
					"converteroptions":{}
				};
				if(conversionOption.outputformat=="png"){
					conversionOption.converteroptions.page_range=SbConfig.maxPagePreview;
				}
				
				function blParam(err, bufferData) {				
					if (err) {
						winston.error(err);
						return reject(null, err);
					}

					let bufferFileType = fileType(bufferData).ext;
					let savePath;
					if (bufferFileType == "zip") {
						targetExt = ".zip";
						savePath = SbConfig.cloudConvertPath;
					} else if (bufferFileType == "png") {
						targetExt = ".png";
						savePath = SbConfig.imagePath;
						fileName = fileName + "-1";
					} else if (bufferFileType == "pdf") {
						targetExt = ".pdf";
						savePath = SbConfig.cloudConvertPath;
					}
					// Hold Data from API response in variable
					let bufferHolder1 = new bl();
					bufferHolder1.append(new Buffer(bufferData));
					
					// If File Already Exists update it with the new file
					if(hlpr.SbFileExist(path.join(savePath, fileName + targetExt)) === true) {
					 	//fs.unlinkSync(filePath);
						let response = {};
						response.status = targetExt.replace("." , "") + "_created";
						return resolve(response);
					}

					bufferHolder1.pipe(fs.createWriteStream(path.join(savePath, fileName + targetExt))
						.on('finish', function () {
							let response = {};
							if (targetExt == ".zip") {
								response.status = "zip_created";
								return resolve(response);
							} else if (targetExt == ".png") {
								response.status = "png_created";
								return resolve(response);
							} else if (targetExt == ".pdf") {
								response.status = "pdf_created";
								return resolve(response);
							}
						})
						.on('error', function (error) {
							console.log("error when writing file =>  ")
							winston.error(error)
							return reject(null, error);
						})
					)

				}

				if (hlpr.getFilenameFromUrl(fileURL).indexOf(".") < 0) {
					conversionOption.inputformat = "website";
					conversionOption.input = "url";
					conversionOption.file = fileURL;
					delete conversionOption.converteroptions;
					return cloudconvert.convert(conversionOption)
					.pipe(bl(blParam))
					.on('error', function(error) {
					    winston.error(error)
						return reject(null, error);
					});
				} else {
					var readStream = fs.createReadStream(filePath);

					readStream.on('open', function () {
						readStream.pipe(cloudconvert.convert(conversionOption))
						.pipe(bl(blParam))
						.on('error', function(error) {
						    winston.error(error)
							return reject(null, error);
						});
					});

					readStream.on('error', function(error) {
						winston.error(error)
						return reject(null, error);
					});
				}
			});
		},

		parseCloudConvertZip: (fileName, imagePath, hlpr, extract, callback) => {
			try {
				let response = {};
				if (hlpr.SbFileExist(SbConfig.cloudConvertPath + fileName + 'zip') === true) {
					let zipFile = new AdmZip(SbConfig.cloudConvertPath + fileName + 'zip');

					response.noOfImages = zipFile.getEntries().length;

					// Extract if flag is set
					if (extract) {
						zipFile.extractAllTo(SbConfig.imagePath, true);
					}

					//fs.unlink(SbConfig.cloudConvertPath + fileName + 'zip' , function(resp,err){});
					return callback(response);
				} else if (hlpr.SbFileExists(imagePath) === true) {
					console.log(imagePath)
					response.noOfImages = 1;
					return callback(response);
				} else {
					return callback(null, "No Zip Archieve present");
				}
			} catch (error) {
				winston.error(error)
				return callback(null, error);
			}
		}
	};
}

