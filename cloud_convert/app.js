'use strict';

const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const winston = require('winston');
const url = require('url');

let helpers = require('./helpers');
let SbConfig = {};

winston.cli();
winston.add(winston.transports.DailyRotateFile, {
    filename: 'logFile.log',
    handleExceptions: true,
    humanReadableUnhandledException: true,
    json: false
});

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

// Allow Cors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header ('Access-Control-Allow-Methods', 'GET,PUT,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.post('/node-app/exportPdf',function(req,res){
    helpers.exportPdf(req.body.docUrl).then(pdfUrl => {
        console.log("pdfUrl: "+pdfUrl);
        res.send({status:200,pdfUrl:pdfUrl});
        })
    .catch(err => {
        winston.error(err);
        res.send({status:500});
        });
        
});
app.get('/node-app/preview', function(req, AppResponse) {
    var fileUrl = req.query.resourceUrl;
    //var fileUrl = 'https://www.swiftview.com/tech/letterlegal5.doc';
    //var fileUrl = 'http://www.easychair.org/publications/easychair.docx';
    //var fileUrl = 'http://berkeleycollege.edu/browser_check/samples/excel.xls';
    //var fileUrl = 'https://sctcc.ims.mnscu.edu/shared/CheckYourComputer/SamplePPTX.pptx';
    //var fileUrl = 'http://www.acdbio.com/sites/default/files/sample.ppt'

    return helpers.downloadFileAndProcess(fileUrl)
        .then(function(response) {
            helpers.SbRender(response, AppResponse);
        })
        .catch(function(err) {
            helpers.SbRender(err, AppResponse);
        });
});

app.get('/node-app/searchImageExist', function(req, AppResponse) {
    var fileName = req.query.fileName;

    helpers.checkIfImageExist(fileName, function(resp, err) {
        let response = {};
        
        if(!resp) {
            response.status = "not_found";
        } else {
            response.fileName = resp;
        }

        helpers.SbRender(response, AppResponse);
    });
});

app.get('/node-app/preview/:fileName/:pageCounter', function (req, AppResponse) {
    var fileName = req.params.fileName;
    var pageCounter = req.params.pageCounter
    var args = [
        fileName,
        pageCounter,
        getPreviousNextImageFn
    ];
    
    return helpers.getPreviousNextImage.apply(helpers, args);

    function getPreviousNextImageFn(err, resp) {
        var response = {};

        if (err) {
            response.status = "error";
        }

        if (resp) {
            if (resp.msg === 'max_reached') {
                response.status = "max_reached";
            } else {
                response.status = "success";
            }
            response.maxPageCount = resp.maxPageCount;
            response.pageCounter = resp.pageCounter;
            response.preview = resp.preview;
        }

        helpers.SbRender(response, AppResponse);
    }
});


app.get('*', function(req, res) {
    res.json({});
});

app.listen(3000, () => {
    winston.info('Service started.');

    // Get Config
    helpers.loadJSON(helpers.SbConfigPath)
        .then(config => SbConfig = config);
});
