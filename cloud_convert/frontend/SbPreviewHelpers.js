(function(sb) {
    'use strict';

    /** Helpers Functions For SearchBlox Preview **/

    /** Next Link in doc **/
    var loadInProgress = 0;
    var toggeleMaximize = "expand", leftPosition, adjustLeftPos;

    sb.ChangeImgInDoc = ChangeImgInDoc;
    sb.togglePopupScreen = togglePopupScreen;
    sb.getFileNameByURL = getFileNameByURL;
    sb.getFileName=getFileName;
    sb.getFileExtByURL = getFileExtByURL;
    sb.getFileNameFromImgPath = getFileNameFromImgPath;
    sb.changeCount = changeCount;
    sb.changePageCountDisplay = changePageCountDisplay;
    sb.acceptableExtension = acceptableExtension;
    sb.acceptableDocumentExtension = acceptableDocumentExtension;
    sb.getExactFilenameFromUrl = getExactFilenameFromUrl;
    
    function ChangeImgInDoc(self,direction) {
        var popover = jQuery(self).parents("div.webui-popover");
        var popoverContent = popover.find("div.webui-popover-content");
        var popoverInnerContent = popoverContent.find("div.webui-popover-inner-content");
        var popupID = popover.attr("id"); // Get Popup ID
        var maxCount = jQuery("div[data-target='"+popupID+"']").attr("data-img-count"); // Get Img Count On the Target Btn
        var pdfPreviewImg = popoverInnerContent.children("img");
        var ImgSrc = pdfPreviewImg.attr("src");
        var count = parseInt(ImgSrc.split('-').pop().split('.').shift()); // Gets Img Count

        if(direction == "prev") {

            if(count < 2){
                return; // No Prev Img
            }
            count--; // Go to Prev Image

            ImgSrc =  changeCount(ImgSrc,count,"dec"); // Replace Old Count With New Count
            pdfPreviewImg.attr("src", ImgSrc);//Set New Image

            /** Change Page Count **/
            changePageCountDisplay(self,count);

            return; // No Need For Ajax Req
        }

        count++; // Go to Next Image

        var fileName = getFileNameFromImgPath(ImgSrc);

        var ReqUrl = sb.SbAppUrl + "preview/" + fileName + "/" + count;

        if(loadInProgress){
            return; // Prevent Further Ajax
        }

        jQuery.ajax({
            url: ReqUrl,
            beforeSend: function() {
                jQuery(self).parents("div.webui-popover").find("img.loader-icon").show(); // Show Loader
                loadInProgress = 1; // Prevent Further Ajax
            }
        })
        .success(function( data ) {
            loadInProgress = 0; // Allow Further Ajax Now
            jQuery(self).parents("div.webui-popover").find("img.loader-icon").hide(); // Hide Loader

            var result = JSON.parse(data);

            if(result && result.status == "success") {
                ImgSrc = changeCount(ImgSrc,count,"inc"); // Replace Old Count With New Count
                pdfPreviewImg.attr("src", ImgSrc);//Set New Image

                /** Change Page Count **/
                changePageCountDisplay(self,count);
            }

            if(result && result.status == "max_reached") {
                console.log("Max Img Count reached" );
            }
        })
        .error(function( err ) {
                popover.find('img.loader-icon').hide(); // Hide Loader
                popoverInnerContent.append(err);
                console.log('Error To Convert to Image', err );
        });
    }

    /** Maximize Popover **/
    function togglePopupScreen(popover, collapse) {
        var popoverContent = popover.find("div.webui-popover-content");
        var popoverInnerContent = popoverContent.find("div.webui-popover-inner-content");
        var toggleType = collapse === true ? 'collapse' : popover.attr('data-popup-screen');


        if (toggleType === 'expand') {
            leftPosition = popover.css('left');  // Save Inital Left Position value
        }

        if (toggleType === 'expand') {
            adjustLeftPos  = leftPosition.match(/\d+/)[0] / 3 ; // Adjust Value for Left Pos
            popover.width(sb.maxWidth).css({left: adjustLeftPos + 'px' });
            if (popoverInnerContent.height() < sb.maxHeight) {
                popoverInnerContent.height(sb.maxHeight);
            }
            popoverInnerContent.css('width', '');
        } else {
            popover.css('width', '').css({left: leftPosition});
            popover.css('height', '');
            popoverInnerContent.css('height', '');
            popoverInnerContent.css('width', '');
        }

        changeToggleState(popover, toggleType);
    }

    function changeToggleState(popover, toggleType) {
        var currentPopupScreen;
        currentPopupScreen = (toggleType === 'expand') ? 'collapse' : 'expand';
        popover.attr('data-popup-screen', currentPopupScreen);
    }

    function getFileName(docUrl) {
        var splittedDocUrl = docUrl.split("/");
        return splittedDocUrl[splittedDocUrl.length-1];
    }

    function getFileNameByURL(srcUrl) {
        var url = new Url(srcUrl);
        var result = url.path.substr(0, (~-url.path.lastIndexOf('.') >>> 0) + 1);
        return result.replace('/', '');
    }

    function getFileExtByURL(srcUrl) {
        var url = new Url(srcUrl);
        return url.path.substr((~-url.path.lastIndexOf('.') >>> 0) + 2);
    }

    function getExactFilenameFromUrl(url) {
        var result=url;
        result = result.substring(result.lastIndexOf("/")+ 1);
        if( (result.match(/[^.]+(\.[^?#]+)?/) || [])[0] !== undefined ) {
            return (result.match(/[^.]+(\.[^?#]+)?/) || [])[0]
        }else{
            //Eg: "http://abc/boom/" -> "boom"
            result=url.substring(0,url.length-1);
            result = result.substring(result.lastIndexOf("/")+ 1);
            return (result.match(/[^.]+(\.[^?#]+)?/) || [])[0]
        }
    }
    
    function getFileNameFromImgPath(ImgSrc) {
        var fileName = getFileName(ImgSrc);
        var removeFromName = fileName.split('-').pop();
        var fileName = fileName.replace("-"+removeFromName, ".");
        return fileName;
    }

    /** Changes Img Counter On Next Click **/

    function changeCount(ImgSrc,count,action) {
        var prevCount;

        if(action == "dec"){
           prevCount = count+1
        } else if(action == "inc"){
            prevCount = count-1
        }
        var pos = ImgSrc.lastIndexOf('-');
        var str_old_count = ImgSrc.substring(pos,ImgSrc.length); // Find The Appropriate portion of string to replce
        var str_new_count = str_old_count.replace(prevCount,count);// Find The Appropriate portion of string to replce

        return ImgSrc.replace(str_old_count,str_new_count);
    }

    function changePageCountDisplay(self,count) {
        var pageCount = parseInt(count);
        //pageCount++;

        jQuery(self).parents("div.webui-popover").find(".current-page-count").html(pageCount)
    }

    function acceptableExtension(extension) {
        return acceptableDocumentExtension(extension) ||
            sb.SbNonDocuments.indexOf(extension) === -1;
    }

    function acceptableDocumentExtension(extension) {
        return sb.SbValidExt.indexOf(extension) > -1;
    }

})(searchBloxPreview);
