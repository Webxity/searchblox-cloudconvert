var searchBloxPreview = {};

(function(sb) {
    'use strict';

    /**
     * Show Resource Previews on Search Page
     */
    var $ = jQuery.noConflict();
    var SbConfig = {};
    
    $.ajax({
        url: "cloud_convert/SbConfig.json",
        async: false
    }).done(function( json ) {
        SbConfig = json;
        SbConfig.SbSearchBloxUrl=window.location.origin+'/';
        SbConfig.SbAppUrl=window.location.protocol+'//'+window.location.hostname+':3000/node-app/';
    });
    
    var SbAppUrl = SbConfig.SbAppUrl;
    var SbValidExt = SbConfig.SbValidExt;
    var SbSearchBloxUrl = SbConfig.SbSearchBloxUrl;
    var SbNonDocuments = SbConfig.nonDocuments;
    var partOfResourcePath="searchblox/plugin/";

    var minWidth = '665px';
    var minHeight = '300px';

    var maxWidth = '1000px';
    var maxHeight = '600px';

    var titleClass = '.result-link';

    var loadInProgress = 0; // Check If Already a preview is in progress
    var checkFileUniqueNo = 0; //Check If same File Called Again
    
    jQuery(document).on('click', '#saver', function (e) {
        e.preventDefault();        
        var file=jQuery(this).attr("data-url");
        var fileName = sb.getFileName(file);
        Dropbox.save(file, fileName);
    }); 
    
    jQuery(document).on('click', '#exportPDF', function(e) {
        e.preventDefault();

        var loader=$('<img id="isExporting" style="width:22px;margin-left:5px" class="popover-loader-icon" src="cloud_convert/frontend/images/preview-loader.gif"/>');
        var theExportBtn=jQuery(this);
        var data = {};
        var docUrl = theExportBtn.parents('td').attr("data-url");
        
        theExportBtn.after(loader);
        theExportBtn.hide();
        
        $.post( SbAppUrl+'exportPdf', {
            docUrl: docUrl,
        },function(response) {
            if(response.status===200 && response.pdfUrl!==undefined){
                loader.hide();
                var file = SbSearchBloxUrl + partOfResourcePath + response.pdfUrl;
                var fileName = sb.getFileName(file);
                
                var desktopBtn = Templates.getDesktopBtn(response.pdfUrl);
                var gdriveBtn = Templates.getGDriveBtn(file, fileName);
                var dropboxBtn = Templates.getDropboxBtn(response.pdfUrl);

                // Insert GDrive, Dropbox, Desktop btns when response comes
                theExportBtn.after(desktopBtn,dropboxBtn,gdriveBtn);
            }else{
                alert("Export To PDF not supported!");   
                loader.hide();
            }
        });

    });

    /** Overrideing Webui Popover Hiding Preview Btn **/
    jQuery(document).on('hide.webui.popover', '*', function(event){
        event.preventDefault();
        jQuery('button.preview-btn').show();
    });

    jQuery(document).on('click', 'a.NextImgInDoc', function(e){
        e.preventDefault();
        sb.ChangeImgInDoc(this, 'next');
    });

    jQuery(document).on('click', 'a.PrevImgInDoc', function(e){
        e.preventDefault();
        sb.ChangeImgInDoc(this, 'prev');
    });

    jQuery(document).on('click', 'a.MaximizePopover', function(e){
        e.preventDefault();
        sb.togglePopupScreen(jQuery(this).parents('div.webui-popover'));
    });

    sb.SbAppUrl = SbAppUrl;
    sb.maxWidth = maxWidth;
    sb.maxHeight = maxHeight;
    sb.SbValidExt = SbValidExt;
    sb.SbNonDocuments = SbNonDocuments;
    sb.SbConfig=SbConfig;
    
    sb.SbAddPreviewBtn = function () {
        var docUrl,fileName,url,extension;

        jQuery(document).find('#facetview_results tr > td').each(function() {
            docUrl = jQuery(this).attr("data-url");

            if (!docUrl) {
                return;
            }

            var fileName = sb.getExactFilenameFromUrl(docUrl);


            extension = sb.getFileExtByURL(docUrl);
            if (!sb.acceptableExtension(extension)) {
                return;
            }

            //Uniquely Identify the btn with this
            var UniqueNo = Math.random();

            var btnWrap = jQuery(this).find('div.preview-btn-wrap');
            btnWrap.attr('data-identify', UniqueNo);

            var result = jQuery(this).find("a#searchresult");
            var resultLink = jQuery(this).find("a#searchresult");

            if (sb.acceptableDocumentExtension(extension)) {
                return result.hoverIntent({
                    over: onHover(result, resultLink, docUrl, UniqueNo),
                    out: onOut(result, resultLink)
                });
            }

            function onHover(targetedElement, targetLink, docUrl, UniqueNo) {
                return function() {
                    if (!targetedElement.hasClass('previewing')) {
                        jQuery("#facetview_results td").find(".previewing").removeClass("previewing");
                        targetedElement.addClass('previewing');
                        SbShowPreview(docUrl, UniqueNo, targetedElement, targetLink);
                    }
                };
            }

            function onOut(targetedElement, targetLink) {
                return function(e) {
                    var cursorOnInnerElements = targetedElement.find(e.target).length;

                    if (!cursorOnInnerElements && targetedElement.hasClass('previewing')) {
                        targetedElement.removeClass('previewing');
                        targetLink.webuiPopover('hide');
                    }
                };
            }
        });
    }

    function SbShowPreview(fileUrl, UniqueNo, targetedElement, targetLink) {
        var targetDiv = targetLink;

        if (checkFileUniqueNo == UniqueNo) {
            // File Already Sent To server dont send the same again until processed
            return showWebUIPopover();
        }

        checkFileUniqueNo = UniqueNo; // Check this on next request

        var SbImgPath = "cloud_convert/resources/resource-images/";
        var orgFileUrl = fileUrl;
        fileUrl = encodeURIComponent(fileUrl);
        var url = SbAppUrl + "preview?resourceUrl=" + fileUrl;
        var count = 0, imgName;

        // Place Pdf File Name On Next and Previous Btn
        var targetUiPopover = jQuery(targetDiv).attr("data-target");
        var pdfFileName = sb.getFileNameByURL(fileUrl);
        pdfFileName = pdfFileName.substr(0, pdfFileName.lastIndexOf(".")) + ".pdf";

        createWebUIPopover(UniqueNo);
        showWebUIPopover();

        jQuery("div#" + targetUiPopover)
            .find(".NextImgInDoc, .PrevImgInDoc")
            .attr("data-file", pdfFileName);

        // Initiate Popover
        function createWebUIPopover(UniqueNo) {
            var resultUrl = jQuery(targetDiv).attr("uid");

            targetDiv.webuiPopover({
                type: 'async',
                url: url,
                closeable: true,
                trigger: 'manual', // Manually Trigger At End
                placement: 'right',
                dismissible: true,
                cache: false,
                abortXHR: false,
                template:
                    '<div class="webui-popover" data-result-url-popover="'+resultUrl+'" data-identify="'+UniqueNo+'" >' +
                        '<div class="arrow"></div>' +
                        '<div class="webui-popover-inner">' +
                            '<img class="popover-loader-icon" src="cloud_convert/frontend/images/preview-loader.gif"/>'+
                            '<div class="content-popover" style="display:none;">'+
                                '<div class="popup-header">' +
                                    '<div class="popover-pagination">' +
                                        '<a href="#" class="PrevImgInDoc"><i class="icon-arrow-left"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'+
                                        '<a href="#" class="NextImgInDoc"><i class="icon-arrow-right"></i></a>'+
                                    '</div>'+
                                    '<div class="popup-actions text-right pull-right">' +
                                        '<a href="#" class="MaximizePopover"><img class="maximize-icon" src="cloud_convert/frontend/images/zoom-icon-small.png"/></a>'+
                                        '<a href="#" class="close">x</a>' +
                                    '</div>' +
                                '</div>' +
                                '<h3 class="webui-popover-title"></h3>' +
                                '<div class="webui-popover-content">' +
                            '</div>'+
                            '<i class="icon-refresh"></i><p>&nbsp;</p></div>' +
                        '</div>' +
                    '</div>',
                async: {
                    before: function (that, xhr) {
                        loadInProgress = 1; // Prevent Further Ajax
                    },
                    success: function (result, err) {
                        checkFileUniqueNo = 0; // Allow Request to same File Now
                        loadInProgress = 0; // Allow further pagination

                        jQuery(document).find("*[data-identify='"+UniqueNo+"']").find(".content-popover").show(); // Show Loader
                        jQuery(document).find("*[data-identify='"+UniqueNo+"']").find(".popover-loader-icon").hide(); // Show Loader

                        // If Target Div is no longer present then remove the Popover
                        if(jQuery(document).find(targetedElement).length == 0) {
                            jQuery(document).find('*[data-result-url-popover="'+orgFileUrl+'"]').remove();
                        }
                    }
                },
                content: function (result) {
                    jQuery(targetedElement).find(".result-loader-icon").hide();

                    checkFileUniqueNo = 0; // Allow Request to same File Now
                    jQuery("div.webui-popover").find(".loader-icon").hide(); // Hide Loader
                    try {
                        var resp = JSON.parse(result);
                    } catch (e) {
                        return "No Resp From App";
                    }

                    if(jQuery(document).find(targetedElement).length == 0) {
                        return false;
                    }

                    if(!resp) {
                        return "No Img Created By App";
                    }

                    if (resp.status !== "converted") {
                        return '<br /><h3 class="text-center">Not found</h3>';
                    }

                    if (resp.status === "converted") {
                        var webpopoverUI = jQuery('#' + targetDiv.data('target'));
                        var fileName = sb.getFileNameByURL(fileUrl);
                        var imgName = resp.preview;

                        var html =
                        "<div class='webui-popover-inner-content'>" +
                            "<img src='" + SbImgPath + imgName + "' style='width: 100%; height: auto;'/>" +
                        "</div>";

                        if (!resp.htmlpreview) {
                            html += "<div class='page-count'>Page <span class='current-page-count'>" +
                            ""+ resp.pageCounter + "</span> of "+ resp.maxPageCount+"</div>";

                            webpopoverUI.find('.popover-pagination').removeClass('hide');
                        }

                        if (resp.htmlpreview) {
                            webpopoverUI.find('.popover-pagination').addClass('hide');
                        }

                        return html;
                    }
                },
                onHide: function() {
                    targetedElement.removeClass('previewing');
                    var webpopoverUI = jQuery('#' + targetDiv.data('target'));
                    sb.togglePopupScreen(webpopoverUI, true);
                },
                onShow: function() {
                    var webpopoverUI = jQuery('#' + targetDiv.data('target'));
                    // if (!webpopoverUI.attr('data-popup-screen')) {
                        //webpopoverUI.attr('data-popup-screen', 'expand');
                    // }

                    // webpopoverUI.resizable({
                    //     handles: 'all',
                    //     alsoResize: '#' + targetDiv.data('target') + ' div.webui-popover-inner-content',
                    //     minHeight: 150,
                    //     minWidth: 170
                    // });
                }
            });
        }

        //Show it
        function showWebUIPopover() {
            targetDiv.webuiPopover('show');
        }
    }
})(searchBloxPreview);
