window.Templates =(function (searchBloxPreview) {
    var Templates={
        getExportBtn:function () {
            return '<button id="exportPDF" type="button" class="btn btn-sm btn-margin">\
                            <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> Export PDF\
                         </button> ';
        },
        getDesktopBtn:function (href) {
            return '<a id="desktop" href='+href+' target="_blank"><button type="button" class="btn btn-sm btn-margin">\
                        <span class="glyphicon glyphicon-download-alt " aria-hidden="true"></span> To Desktop\
                     </button></a> ';
        },
        getDropboxBtn:function (url) {
            return '<a id="saver" href="#" data-url='+url+' class="dropbox-saver"><button type="button" class="btn btn-sm btn-margin">\
                        <span><i class="icon-dropbox icon-white"></i></span><span class="dropin-btn-status"></span>Save to Dropbox\
                     </button></a> ';
        },
        getGDriveBtn:function (src,filename) {
            return '<div class="g-savetodrive btn-margin"\
                            data-src='+src+'\
                            data-filename='+filename+'\
                            data-sitename="SearchBlox">\
                        </div>\
                        <script type="text/javascript">\
                        gapi.savetodrive.go("container");\
                        </script>';
        }
    }
    
    return Templates;
})(searchBloxPreview);