'use strict';

/* global module, require */

const fs = require('fs');
const path = require('path');
const http = require('follow-redirects').http;
const https = require('follow-redirects').https;
const url = require('url');
const winston = require('winston');
const webshot = require('webshot');
const crypto = require('crypto');
const glob = require('glob');
const validUrl = require('valid-url');
const async = require('async');

const SbConfigPath = 'SbConfig.json';
const fileData = fs.readFileSync(SbConfigPath, 'utf8');
const SbConfig = JSON.parse(fileData);
const batchJob = false;

let zipCreated = false;
let hlpr = {};

/**
 * Render APP Response
 */
hlpr.SbRender = function(response,res) {
    res.send(JSON.stringify(response))
};

hlpr.SbConfigPath = SbConfigPath;
hlpr.DownloadFileHttpGet = DownloadFileHttpGet;
hlpr.ProcessResource = ProcessResource;
hlpr.ProcessResourceConversion = ProcessResourceConversion;
hlpr.downloadFileAndProcess = downloadFileAndProcess;
hlpr.SbFileExists = SbFileExists;
hlpr.SbFileExist = SbFileExist;
hlpr.isHtmlDoc = isHtmlDoc;
hlpr.loadJSON = loadJSON;
hlpr.fileExtension = fileExtension;
hlpr.globSearch = globSearch;
hlpr.batchJob = batchJob;
hlpr.zipCreated = zipCreated;
hlpr.getPreviousNextImage  = getPreviousNextImage;
hlpr.getFilenameFromUrl=getFilenameFromUrl;
hlpr.exportPdf = exportPdf;
hlpr.checkIfImageExist = checkIfImageExist;

const cloudConvertApi = require('./cloudConvertApi')(SbConfig,hlpr);


function exportPdf(docUrl){
    return downloadFile(docUrl, true,true)
        .then(function(downloadInfo) {
            var fileName= getFilenameFromUrl(downloadInfo.file);
            var fileExt = fileExtension(fileName);
            var fileNameWithOutExtAndSpaces = fileName.replace("."+fileExt,".").replace(/\s/g,"");;
            var downloadFilePath =SbConfig.resourcePath+fileName;
            var convertFilePath = SbConfig.cloudConvertPath+fileNameWithOutExtAndSpaces+"pdf";
            var fileExistsInHtmlOutput=SbFileExists(downloadFilePath);
            var fileExistsIntemporaryPDF=SbFileExists(convertFilePath);
            
            // If File is already PDf don't convert
            if(fileExt==="pdf"){
                return "cloud_convert/"+downloadFilePath;
            }
            
            if(fileExistsIntemporaryPDF!==true||fileExistsInHtmlOutput!==true||downloadInfo.updateRequire) {
                return cloudConvertApi.uploadAndSaveConvertedFile(downloadFilePath,fileNameWithOutExtAndSpaces,"upload",fileExt, "pdf" , ".pdf",docUrl)
                    .then(response => {
                        winston.info(response);
                        return "cloud_convert/"+convertFilePath;
                    })
                    .catch(err => {
                        winston.error(err);
                        return Promise.reject(err);
                    });
            } else {
                return "cloud_convert/"+convertFilePath;
            }

        }).catch(err => {
            winston.error(err);
        });
}

function checkIfImageExist(fileName,callback) {

    let fileExt = `.${fileExtension(fileName)}`;
    let fileWithoutExt = fileName.replace(fileExt, '');
    let error = {};
   
    if(!acceptableDocumentExtension(fileExtension(fileName))) {
        callback(null);
    }
    
    let matchingFile = `${SbConfig.imagePath + fileWithoutExt}_*${".png"}`;

    return globSearch(matchingFile)
    .then(files => {
        if (!files || !files.length) callback(null);

        let newFilePath = files[0];
        if(newFilePath) {
            callback(newFilePath);
        } else {
            callback(null);
        }
    })
    .catch(err => {
        callback(null);
    });
}

/**
 * Function to save given file using HTTP.get
 */
function DownloadFileHttpGet(fileUrl, callback, checkForUpdate,isExportPdf) {
    fileUrl = decodeURIComponent(fileUrl);
    let _isHTMLDoc = isHtmlDoc(fileUrl);
    let _isAcceptableDoc = acceptableDocumentExtension(fileExtension(fileUrl));
    let _validDocsAndProceed = (_isHTMLDoc ) || _isAcceptableDoc;
    let fileName;
    let fileNameWithOutExt;
    let fileExt;
    let fileWithoutExt;
    let fileSysCollection;
    let newFilePath;
    let resourcePath;
    let urlParsed = url.parse(fileUrl);

    if (!validUrl.isWebUri(fileUrl)) {
        if(SbFileExists(fileUrl) !== true) {
            return callback(`invalid url: ${fileUrl}`);
        } else {
            // It's a File System Collection
            fileSysCollection = true;
        }
    }

    if (!_validDocsAndProceed) {
        return callback('invalid document type');
    }

    // if (_isAcceptableDoc && !_isHTMLDoc) {
    //     resourcePath = SbConfig.resourcePath;
    //     fileName = urlParsed.pathname.split('/').pop();
    // }

    resourcePath = SbConfig.resourcePath;
    fileName = getFilenameFromUrl(fileUrl);

    fileExt = `.${fileExtension(fileName)}`;

    // Change file extension to 'html' if there it is equal to 'htm' or not present 
    if(fileExt==="."||fileExt===".htm")fileExt=".html"

    fileWithoutExt = fileName.replace(fileExt, '');
    fileWithoutExt = fileWithoutExt.replace(/[^a-zA-Z]/g, "");    

    let fullFilePath = resourcePath + fileName;
    let matchingFile = `${resourcePath + fileWithoutExt}_*${fileExt}`;
    let updateRequire = false;
    let fileAlreadyExist = false;

    return globSearch(matchingFile)
        .then(files => {
            if (!files || !files.length) return goForDownload();

            winston.info(`<${fileName}> Already Exists`);

            fileAlreadyExist = true;
            newFilePath = files[0];
            fileName = path.basename(newFilePath);
            fileExt = `.${fileExtension(fileName)}`;

            if (checkForUpdate === true) {
                winston.info(`update status: trigger for ${fileName}`);
                return goForDownload();
            }
            return callback(null, fileName);
        })
        .catch(err => {
            winston.error(err);
            return goForDownload();
        });

    function goForDownload() {
        // Check if File on Local system or a Http Url
        if(!fileSysCollection) {
            let protocol = url.parse(fileUrl).protocol;
            let requested_protocol = (protocol === 'https:') ? https : http;
            let request = requested_protocol.get(fileUrl, ResponseRequest);

            winston.info(`http request executed`);

            request.on('error', err => {
                winston.error(err);
                winston.warn(`unable to get file from ${fileUrl}`);
                callback(`unable to get file from ${fileUrl}`);
            });
        } else {
            let readStream = fs.createReadStream(fileUrl);
            readStream.on('open', function () {
                ResponseRequest(readStream)
            });
            readStream.on('error', function(err) {
                winston.error(err);
                winston.warn(`unable to get file from ${fileUrl}`);
                callback(`unable to get file from ${fileUrl}`);
            });
        }
        
        function ResponseRequest(res) {
            winston.info(`downloading file <${fileName}>`);

            return writeFile(res, fullFilePath)
                .then(() => {
                    winston.info(`<${fileName}> downloaded, extracting file hash...`);
                    return getFileHash(fullFilePath);
                })
                .then(newHash => {
                    if (fileAlreadyExist === true && checkForUpdate === true) {
                        winston.info('update status: comparing hash');

                        return getFileHash(newFilePath)
                            .then(oldHash => {
                                if (oldHash !== newHash) {
                                    updateRequire = true;
                                    winston.info(`update status: available, deleting old file <${fileName}>`);
                                    return deleteFile(newFilePath)
                                        .then(() => _fileHashRename(newHash))
                                }

                                winston.info(`update status: not available, preserving old file <${fileName}>`);
                                return deleteFile(fullFilePath);
                            });
                    }

                    return _fileHashRename(newHash);
                })
                .then(() => callback(null, fileName, updateRequire))
                .catch(err => {
                    winston.error(err);
                    return callback(err);
                });

            function _fileHashRename(hash) {
                winston.info(`<${fileName}> hash = ${hash}`);
                winston.info('renaming with concatenated hash');

                fileName = `${fileWithoutExt}_${hash + fileExt}`;
                fileNameWithOutExt = `${fileWithoutExt}_${hash}`;
                newFilePath = resourcePath + fileName;
                return renameFile(fullFilePath, newFilePath)
                    .then(() => {
                        winston.info(`<${fileName}> moved to ${resourcePath}`);
                        var outputExt="png",targetExt=".zip";
                        if(isExportPdf) { 
                            outputExt="pdf";
                            targetExt=".pdf"; 
                        }

                        return cloudConvertApi.uploadAndSaveConvertedFile(newFilePath,fileNameWithOutExt,
                            "upload",fileExt, outputExt , targetExt,fileUrl)
                            .then(response => {
                                winston.info(response)
                                if(response.status == "zip_created") {
                                    hlpr.zipCreated = true;
                                } else if (response.status == "png_created") {
                                    hlpr.zipCreated = false;
                                } else if (response.status == "pdf_created") {
                                    hlpr.zipCreated = false;
                                }

                                return Promise.resolve(response);
                            })
                            .catch(err => {
                                winston.error(err);
                                return Promise.reject(err);
                            });
                    });
            }
        }
    }
}

/**
 * Process the saved resource and generate images as output
 */
function ProcessResource(fileurl, fileName, htmlConversion, updateRequire, callbackParent) {
    try {
        var resourcePath = SbConfig.resourcePath;
        var filePath = path.join(resourcePath, fileName);
        var ext = fileExtension(fileName);
        var fileNameWithOutExt = fileName.replace(ext,"");
        var response = {}; // Resp for the request

        let pageCounter = 1;
        let imageName = getImageNameFromPdf(fileNameWithOutExt + "png", pageCounter);
        let imagePath;
        imagePath = path.join(SbConfig.imagePath, imageName);

        async.series([
            function(callback) {
                  if(SbFileExists(imagePath) !== true && !hlpr.zipCreated || (checkIfZipExists(fileNameWithOutExt) !== true) ) {
                    fs.unlinkSync(filePath);
                    downloadFile(fileurl)
                        .then(function(downloadInfo) {
                            var fileName = downloadInfo.file;
                            var filePath = path.join(resourcePath, fileName);
                            var ext = fileExtension(fileName);
                            var fileNameWithOutExt = fileName.replace(ext,"");
                            var imageName = getImageNameFromPdf(fileNameWithOutExt + "png", pageCounter);
                            callback(null, "File Downlaoded Again now process")
                    });
                } else {
                    callback(null,"first step clear proceed now");
                }
            },
            function(callback) {
                response = SbFileExists(filePath);

                // If file not found, don't process
                if (response !== true) {
                    return callback(filePath + " => File Not Found",null);
                }

                response = {};

                var resourceFile = filePath.substr(0, filePath.lastIndexOf(".")) + '.pdf';

                winston.info('Resource processed');

                imagePath = path.join(SbConfig.imagePath, imageName);

                // If Image Already exists then show image
                if(SbFileExists(imagePath) === true ) {

                    cloudConvertApi.parseCloudConvertZip(fileNameWithOutExt, imagePath, hlpr, false, function(response, error) {
                            if (error) {
                                return callback(error,null);
                            }

                            response.status = 'converted';
                            response.maxPageCount = response.noOfImages;
                            response.pageCounter = pageCounter;
                            response.preview = imageName;

                            callback(null,response);
                        }
                    );
                } else if (hlpr.zipCreated || (checkIfZipExists(fileNameWithOutExt) === true)) {

                    cloudConvertApi.parseCloudConvertZip(fileNameWithOutExt, imagePath, hlpr, true, function(response, error) {
                            if (error) {
                                return callback(error,null);
                            }

                            response.status = 'converted';
                            response.maxPageCount = response.noOfImages;
                            response.pageCounter = pageCounter;
                            response.preview = imageName;
                            callback(null,response);
                        }
                    );
                } else {
                    let error = "Image or Zip not found for the document " + filePath;
                    winston.error(error);
                    return callback(error,null);
                }
            }
        ],
            function(err,results){
                if(err) {
                    winston.error(err)
                    return callbackParent(null, err)
                } else {
                    winston.info(results)
                    return callbackParent(results[1])
                }
            }
        );
    } catch(err) {
        winston.error(err)
        return callback(null, err)
    }
}

function downloadFile(fileUrl, checkForUpdate,isExportPdf) {
    return new Promise(downloadFilePromise);

    function downloadFilePromise(resolve, reject) {
        DownloadFileHttpGet(fileUrl, function(err, fileName, updateRequire) {
            if (err) {
                return reject({
                    status: "error",
                    message: err
                });
            }

            resolve({file: fileName, updateRequire: updateRequire});
        }, checkForUpdate,isExportPdf);
    }
}

function ProcessResourceConversion(fileurl, filename, htmlconversion, updateRequire) {
    return new Promise(ProcessResourcePromise);

    function ProcessResourcePromise(resolve, reject) {
        ProcessResource(fileurl, filename, htmlconversion, updateRequire, function(response, error) {
            if (error) {
                return reject(error);
            }
            winston.info("now rendering");
            resolve(response);
        });
    }
}

function downloadFileAndProcess(fileurl, htmlconversion, checkForUpdate) {
    return downloadFile(fileurl, checkForUpdate)
        .then(function(downloadInfo) {
            return ProcessResourceConversion(fileurl,downloadInfo.file, htmlconversion, downloadInfo.updateRequire);
        });
}

function getPreviousNextImage(fileName, pageCounter, callback) {
    // If no Counter then First Image i.e 1
    if (!pageCounter) {
        pageCounter = 1;
    }

    let firstImageName = getImageNameFromPdf(fileName + "png", 1);
    var response = {};

    let zipFile = SbConfig.cloudConvertPath + fileName + 'zip'

    if(SbFileExists(zipFile) === true ) {
        response.status = 'converted';

        cloudConvertApi.parseCloudConvertZip(fileName, firstImageName, hlpr, false, function(resp, error) {
                if (error) {
                    return callback(error);
                }

                // Return If Counter Increases the Max Size
                if (pageCounter > resp.noOfImages) {
                    response.msg = 'max_reached';
                    winston.info('no next image');
                    return callback(null, response);
                }

                let imageName = getImageNameFromPdf(fileName + "png", pageCounter);
                response.status = 'converted';
                response.maxPageCount = resp.noOfImages;
                response.pageCounter = pageCounter;
                response.preview = imageName;
                return callback(null, response);
            }
        );
    } else if(hlpr.SbFileExist(SbConfig.imagePath + firstImageName) === true) {
        response.status = 'converted';
        response.maxPageCount = 1;
        
        // Return If Counter Increases the Max Size
        if (pageCounter >  response.maxPageCount) {
            response.msg = 'max_reached';
            winston.info('no next image');
            return callback(null, response);
        }
        
        response.pageCounter = 1;
        response.preview = firstImageName;
        return callback(null, response);
    } else {
        let error = "No Zip archieve found for images";
        return callback(error);
    }
}

/**
 * Check If file Exist
 */
function SbFileExists(filePath) {
    var response = {};
    response.status = `not found: ${filePath}`;
    try {
        return SbFileExist(filePath) ? true : response;
    } catch(e) {
        return e;
    }
}

/**
 * Gets Img Name From PDf File name
 */

function getImageNameFromPdf(fileName,pageCounter) {
    var pdfFile = fileName.substr(0, fileName.lastIndexOf("."));
    return pdfFile + "-" + pageCounter + ".png";
}

function SbFileExist(filePath) {
    try {
        if((fs.lstatSync(filePath).isFile()) === true) {
            return true;
        } else {
            return false;
        }
    } catch(e) {
        return false;
    }
}

function isHtmlDoc(filename) {
    // E.g isHtmlDoc("https://sv.wikipedia.org/wiki/den_gode_herden") -> true
    // E.g isHtmlDoc("https://sv.wikipedia.org/wiki/den_gode_herden/") -> true
    // E.g isHtmlDoc("http://docs.fdrlibrary.marist.edu/psf/box31/t297c01.html") -> true
    var a=filename.slice(-1);
    return (filename.slice(-1)==="/"||filename.slice(-1)===""||filename.indexOf('.html') > -1 || filename.indexOf('.htm') > -1) ||
        (!acceptableDocumentExtension(fileExtension(filename)) &&
        SbConfig.nonDocuments.indexOf(fileExtension(filename)) === -1);
}

function fileExtension(fileName) {
    return path.extname(fileName).slice(1);
}

function windowsSafeFileName(filename) {
    filename = decodeURIComponent(filename);
    return filename.replace(/:|\*|\?|\/|\\|"|<|>|\||&|\?/g, '-');
}

function acceptableDocumentExtension(extension) {
    return SbConfig.validExt.indexOf(extension) > -1 ||
        extension === 'pdf';
}

function loadJSON(configFile) {
    return new Promise((resolve, reject) => {
        fs.readFile(configFile, 'utf8', (err, data) => {
            if (err) {
                return reject(err);
            }

            try {
                let config = JSON.parse(data);
                resolve(config);
            } catch (e) {
                reject(e);
            }
        });
    });
}

function writeFile(stream, filepath) {
    return new Promise((resolve, reject) => {
        let file = fs.createWriteStream(filepath);
        stream.pipe(file);

        file.on('finish', () => file.close(() => resolve(filepath)));
        file.on('error', reject);
    });
}

function renameFile(oldPath, newPath) {
    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, err => {
            if (err) return reject(err);
            resolve(newPath);
        });
    });
}

function deleteFile(filepath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filepath, err => {
            if (err) return reject(err);
            resolve();
        });
    });
}

function getFileHash(filepath) {
    return new Promise((resolve, reject) => {
        // the file you want to get the hash
        var fd = fs.createReadStream(filepath);
        var hash = crypto.createHash('sha1');
        hash.setEncoding('hex');

        fd.on('end', () => {
            hash.end();
            resolve(hash.read()); // the desired sha1sum
        });

        fd.on('error', reject);

        // read all file and pipe it (write it) to the hash object
        fd.pipe(hash);
    });
}

function globSearch(filepath) {
    return new Promise((resolve, reject) => {
        glob(filepath, (err, files) => {
            if (err) return reject(err);
            resolve(files);
        });
    });
}

//Eg: "http://abc/boom.html" -> "boom.html"
//Eg: "http://abc/boom" -> "boom"
//Eg: "C:\Bitnami\wordpress-4.4.2-2\apache2\htzdocs\files\apple.xls" -> "apple.xls"
function getFilenameFromUrl(url){
    var result=url;
    result = result.substring(result.lastIndexOf("/")+ 1);
    result = result.substring(result.lastIndexOf("\\")+ 1);
    if( (result.match(/[^.]+(\.[^?#]+)?/) || [])[0] !== undefined ) {
        return (result.match(/[^.]+(\.[^?#]+)?/) || [])[0]
    }else{
        //Eg: "http://abc/boom/" -> "boom"
        result=url.substring(0,url.length-1);
        result = result.substring(result.lastIndexOf("/")+ 1);
        return (result.match(/[^.]+(\.[^?#]+)?/) || [])[0]
    }
}

//Create Conversion object depending on document url
//E.g getConversionObjectForConvertingToPDF("http://alistapart.com/articles/boom")
//   -> {
//          "inputformat": "website",
//          "outputformat": "pdf",
//          "input": "url",
//          "file": "http://alistapart.com/articles/boom",
//       })
function getConversionObjectForConvertingToPDF(docUrl){
    var inputformat = fileExtension(docUrl);
    var conversionOption = {
        "outputformat": "pdf",
        "file": docUrl
    };
    if(acceptableDocumentExtension(inputformat)) {
        conversionOption.inputformat = inputformat;
        conversionOption.input = "download";
    }
    // when inputformat=='xml'||inputformat==website||inputformat==undefined
    else{
        conversionOption.inputformat = "website";
        conversionOption.input = "url";
    }
    return conversionOption;
}

function removeSpecialCharacters(str){
    return str.replace(/[^a-zA-Z ]/g, "");
}
function checkIfZipExists(fileNameWithOutExt) {
    return SbFileExist(SbConfig.cloudConvertPath + fileNameWithOutExt + 'zip');
}
module.exports = hlpr;

