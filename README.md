# searchblox-cloudconvert

### Requirements
You will need to install nodejs on your system to get this plugin work
https://nodejs.org/en/download/
Select your specific platform and install nodejs on your system

### Steps
* Download the plugin folder and replace it in the directory `webapps/searchblox`
* Navigate to `plugin/cloud_convert` through command line and run the following cmd `node app.js`
* That's it you will be able to preview the documet in your search results now

### Configuration 

* `maxPagePreview `: To change the number of images appear in preview for a search result, you need to modify this param placed in the config file
`plugin/cloud_convert/Sbconfig.json`. Modify it to the desired range. E.g 1-20
To view the first 20 Images

    ### Cloud Convert
    
    > `apiKey`: Log in to your account at https://cloudconvert.com, click on Dashboard tab, copy your API key. Then open `plugin/cloud_convert/Sbconfig.json` file in your project, and paste your copied key into 'apiKey' attributes's value.

    ### Dropbox
    
    > `data-app-key`: To get Dropbox app key, first you need to Create a new app on the Dropbox Platform. Goto this link https://www.dropbox.com/developers/apps,
    > Select 'Dropbox Api' from first option, 'Full Dropbox' from second option, name your app and click 'Create App' button. Page will be redirected to your App Settings page. Add your domain name on 'Chooser/Saver domains' field. 
    
    >    Now Copy your 'App key' and place it here in the `index.html` file in the root directory.
    `  <script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="Your APP Key Here"></script>`
    
     